public abstract class AbstractShape implements IShape {
    @Override
    public int getPosition() {
        return 0;
    }

    public abstract String getShapeName();
}
