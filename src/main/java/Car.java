import java.awt.*;

public class Car {

    private float fuelLevel;
    private byte gear;
    private Color color;
    private float speed;
    private float currentDirection;
    private float MAX_SPEED = 200;
    private byte MAX_GEAR = 5;

    public void accelerate(float speedDelta) {
        float increasedSpeed = speed + speedDelta;
        if (increasedSpeed <= MAX_SPEED) {
            speed = increasedSpeed;
        }
    }

    public void deccelerate(float speedDelta) {
        float decreasedSpeed = speed - speedDelta;
        if (decreasedSpeed >= 0) {
            speed -= speedDelta;
        }
    }

    public void gearUp() {
        byte increasedGear = (byte)(gear + 1);
        System.out.println("Incr gear: " + increasedGear);
        if (increasedGear <= MAX_GEAR) {
            gear++;
        }
        System.out.println("Gear is " + gear);
    }

    public void gearDown() {
        byte decreasedGear = (byte)(gear - 1);
        if (decreasedGear >= 0) {
            gear--;
        }
    }

    public void steer(float angle) {
        gearDown();
        currentDirection += angle;
        deccelerate(5);
    }

    public void setGear(byte gear) {
        if (gear > MAX_GEAR || gear <0) {
            System.out.println("Sorry the gear entered is not supported ! Please buy a new car !");
        }
        else {
            this.gear = gear;
        }
        /*if (gear <= MAX_GEAR && gear >=0) {
            this.gear = gear;
        }*/
    }

    public byte getGear() {
        return this.gear;
    }

    public Car(float fuelLevel, byte gear, Color color, float speed, float currentDirection, float MAX_SPEED, byte MAX_GEAR) {
        this.fuelLevel = fuelLevel;
        this.gear = gear;
        this.color = color;
        this.speed = speed;
        this.currentDirection = currentDirection;
        this.MAX_SPEED = MAX_SPEED;
        this.MAX_GEAR = MAX_GEAR;
    }

    public Car() {

    }
}
