public class Triangle extends Shape{

    public void flipVertical() {
        System.out.println("Flipping the triangle vertically");
    }

    public void flipHorizontal() {
        System.out.println("Flipping the triangle horizontally");
    }
}
