public class Rectangle extends Shape {

    private int width;
    private int height;

    public int getArea() {
        System.out.println("Area of the Rectangle is ..." + width * height);
        return width * height;
    }

    public Rectangle() {

    }

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }
}
