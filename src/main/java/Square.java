public class Square extends Rectangle {

    private int l;

    public Square() {

    }

    public Square(int l) {
        this.l=l;
        // this calls the constructor from Rectangle and MUST be placed the first statement
        //super(l,l);
    }

    @Override
    public int getArea() {
        System.out.println("Area of the square is " + l*l);
        return l*l;
    }
}
