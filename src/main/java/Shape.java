public class Shape extends AbstractShape{

    public void draw() {
        System.out.println("Drawing a shape");
    }

    public void erase() {
        System.out.println("Erasing the shape");
    }

    @Override
    public String getShapeName() {
        return "";
    }

    public void getColor() {
        System.out.println("Color of the shape is ...");
    }

    public void setColor() {
        System.out.println("Setting new color to shape");
    }

    protected void shapeProtectedMethod() {
        System.out.println("This is the protected method of the shape");
    }

    @Override
    public String toString() {
        return "My awsome shape is " + super.toString();
    }
}
