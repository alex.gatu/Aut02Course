import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class MainClass
{
    public static void oopDemo() {
        // OOP 2
        DemoClass dc = new DemoClass();
        System.out.println(Double.parseDouble("1234.56"));

        Car c1 = new Car();
        c1.setGear((byte)4);
        c1.gearUp();
        c1.accelerate(10);
        c1.steer(11);
        System.out.println(c1.getGear());
        System.out.println(Utils.connectionString);
        Utils u1 = new Utils();
        Utils.len = 3;
        System.out.println(Utils.computeCubeVolume());
        Utils.len = 7;
        System.out.println(Utils.computeCubeVolume());
        //System.out.println(u1.computeCubeVolume(3));

        // OOP 3
        Motorcicle m1 = new Motorcicle();
        Object obj = new Object();
        Circle circle1 = new Circle();
        circle1.draw();
        circle1.getArea();
        circle1.erase();

        Triangle tr1 = new Triangle();
        tr1.flipHorizontal();

        Square sq1 = new Square();
        sq1.draw();
        sq1.getArea();
        sq1.erase();
        sq1.shapeProtectedMethod();

        Rectangle rct1 = new Rectangle();
        rct1.shapeProtectedMethod();
        int x = rct1.getArea();

        Rectangle rct2 = new Rectangle(2,6);
        int y = rct2.getArea();

        sq1.getArea();

        Square sq2 = new Square(2);
        sq2.getArea();

        //System.out.println(circle1.toString()+ "\n" + rct1.toString() +
        //"\n" + rct2.toString() + "\n" +  sq1.toString() + "\n" + sq2.toString());

        Shape s1 = new Circle();
        Shape s2 = new Square();
        Shape s3 = new Triangle();

        System.out.println(s1.toString());
        ((Triangle)s3).flipHorizontal();
    }

    public static void main(String[] args) {

        int[] numbers = { 1, 56, 72, 101, 34 , 69, 87};
        Car[] cars = {new Car(),
                new Car(10.0f, Byte.parseByte("1"),
                        Color.WHITE, 10.0f, 1.0f,
                        10, Byte.parseByte("10"))};
        Square s = new Square(20);
        Square[] squares = { new Square(2), new Square(16) , s};

        Square[] square2 = new Square[5];
        square2[0] = new Square(7);
        square2[2] = new Square(8);
        //square2[6] = new Square(8);

        String[] carsSimple = { "Dacia", "VW", "Mercedes"};
        //System.out.println(carsSimple[1]);
        System.out.println("Length of array is " + carsSimple.length);

        for ( String s2: carsSimple) {
            System.out.println(s2);
        }
        System.out.println("-----------------");
        for (int i =0 ; i < carsSimple.length; i++) {
            System.out.println(carsSimple[i]);
        }

        int[][] sudoku = {
                {1, 2, 7, 8, 3},
                {0, 3, 5, 6, 7},
                {3, 4, 8, 1, 6},
                {2, 8, 1, 9, 4}};
        for (int[] line: sudoku) {
            for (int val : line) {
                System.out.print(val + " ");
            }
            System.out.println();

        }
        System.out.println("-----------------");
        for (int i = 0; i < sudoku.length; i++) {
            for(int j = 0; j < sudoku[i].length; j++) {
                System.out.print(sudoku[i][j] + " ");
            }
            System.out.println();
        }

        ArrayList<String> carList = new ArrayList<String>();
        ArrayList<Integer> values = new ArrayList<Integer>();
        System.out.println("Collection size is " + carList.size());
        carList.add("Dacia");
        carList.add("VW");
        carList.add("Mercedes");
        System.out.println("Collection size is " + carList.size());
        if(carList.contains("Dacia")) {
            System.out.println("List contains !!");
        }

        ArrayList<ArrayList<Integer>> matrix = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer> line1 = new ArrayList<Integer>();
        line1.add(1);
        line1.add(2);
        line1.add(7);
        line1.add(8);
        line1.add(3);
        matrix.add(line1);
        System.out.println(line1.get(0));

//        List l = new List();
////        l.add("abc");
////        l.add("cde");

        for (String car: carList) {
            System.out.println(car);
        }
        System.out.println("-----------------");
        // array
        for (int i =0 ; i < carsSimple.length; i++) {
            System.out.println(carsSimple[i]);
        }
        // array list
        for (int i =0 ; i< carList.size(); i++) {
            System.out.println(carList.get(i));
        }
        // Array
        System.out.println(carsSimple);
        // arrayList
        System.out.println(carList);

        Iterator it = carList.iterator();
        while(it.hasNext()) {
            System.out.println(it.next());
        }
        //carList.remove(carList.get(carList.size()-1));
        carList.remove(1);
        System.out.println(carList);

        ArrayList<Integer> evens = new ArrayList<Integer>();
        for (int i = 0 ; i < 100; i++) {
            if(i% 2 == 0) {
                evens.add(i);
            }
        }
        System.out.println(evens);

        HashSet<String> carSet = new HashSet<String>();
        carSet.add("Dacia");
        carSet.add("VW");
        carSet.add("Mercedes");
        carSet.add("AB");
        System.out.println(carSet);
        for (String hs : carSet) {
            System.out.println(hs);
        }
        HashMap<String, ArrayList<String>> masini = new HashMap<String, ArrayList<String>>();
        ArrayList<String> m = new ArrayList<String>();
        m.add("Logan");
        m.add("Duster");
        m.add("1310");
        masini.put("Dacia", m);
        ArrayList<String> m2 = new ArrayList<String>();
        m2.add("Golf");
        m2.add("Passat");
        m2.add("Tiguan");
        masini.put("VW", m2);
        //m.clear();
        ArrayList<String> m3 = new ArrayList<String>();
        m3.add("CLM");
        m3.add("CLK");
        m3.add("GLE");
        //System.out.println(m3);
        masini.put("Mercedes", m3);
        //m.clear();
        System.out.println(masini);
        // dimensiunea
        System.out.println(masini.size());
        System.out.println(masini.remove("Dacia"));
        System.out.println(masini);
        masini.get("Mercedes").remove("GLE");
        //masini.get("ABC").remove("GLE");
        System.out.println(masini);
        System.out.println(masini.keySet());
        masini.get("VW").add("Polo");
        System.out.println(masini.get("VW"));
        masini.put("Opel", new ArrayList<String>());
        System.out.println(masini);

        for (String key :masini.keySet()) {
            System.out.println("KEY: " + key);
            System.out.println(masini.get(key));
        }
        System.out.println(masini.values());

        Iterator<String> iter = masini.keySet().iterator();
        while(iter.hasNext()) {
            // iter.next() este cheia din fiecare iteratie si se muta pe urmatoarea cheie
            String key = iter.next();
            System.out.println("Key: " + key);
            System.out.println("Values : " + masini.get(key));
        }

        HashMap<String, String> indicatives = new HashMap<String, String>();
        indicatives.put("RO", "Romania");
        indicatives.put("BG", "Bulgaria");
        indicatives.put("MD", "Moldova");
        System.out.println(indicatives.keySet());
        System.out.println(indicatives.values());
        Browser b = Browser.IE;
        if(args[0].contains("Chrome")) {
            b=Browser.CHROME;
        }
        if(args[0].contains("Firefox")) {
            b=Browser.FIREFOX;
        }
        System.out.println("Browserul selectat este: " + b);
    }
}
